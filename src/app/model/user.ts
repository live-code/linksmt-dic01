export interface User {
  id: number;
  name: string;
  age?: number;
  birthday?: number;
  bitcoins?: number;
  gender?: string;
  city?: string;
}
