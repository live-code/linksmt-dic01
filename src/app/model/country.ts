export interface Country {
  id: number;
  name: string;
  desc: string;
}
