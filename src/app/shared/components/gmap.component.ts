import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gmap',
  template: `
    <div class="p-1">
      <div *ngIf="showAddress">{{address}}</div>
      <img
        width="100%"
        [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + address + '&zoom=13&size=400x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </div>
  `,
  styles: [
  ]
})
export class GmapComponent {
  @Input() address: string;
  @Input() showAddress = true;
}
