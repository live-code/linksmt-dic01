import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Country } from '../../model/country';


@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item" 
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link" 
          [ngClass]="{ active: item.id === active?.id}"
        >{{item.name}}</a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: Country[];
  @Output() tabClick: EventEmitter<Country> = new EventEmitter<Country>();
  @Input() active: Country;
}
