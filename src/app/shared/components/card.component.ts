import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div 
        class="card-header"
        [ngClass]="{
          'bg-dark text-white': theme === 'dark',
          'bg-info': theme === 'light'
        }"
        (click)="opened = !opened"
      >
        {{label}}
        
        <i [ngClass]="icon"
           class="pull-right" 
           (click)="iconClickHandler($event)"></i>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content> 
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() label = 'My COmponent';
  @Input() theme: 'dark' | 'light';
  @Input() icon: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>();
  opened = true;

  iconClickHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit()
  }
}
