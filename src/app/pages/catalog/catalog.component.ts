import { Component } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-catalog',
  template: `
    <form #f="ngForm" (submit)="usersService.addUser(f)">
      <!--      <div *ngIf="inputName.errors?.required">Campo Obbligatorio</div>
            <div *ngIf="inputName.errors?.minlength">
              Campo troppo corto. Mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} caratteri
            </div>-->
      <input
        type="text"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid }"
        [ngModel]
        name="name"
        required
        minlength="5"
        #inputName="ngModel"
      >

      <input type="text"
             [ngClass]="{'is-invalid': inputAge.invalid && f.dirty, 'is-valid': inputAge.valid }"
             [ngModel] name="age" required class="form-control is-valid" #inputAge="ngModel">

      <select
        class="form-control"
        [ngModel] name="gender" required
        #inputGender="ngModel"
        [ngClass]="{'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid }"
      >
        <option [ngValue]="null">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      <button type="submit" [disabled]="f.invalid">ADD</button>
    </form>

    <div class="container mt-3">
      <div 
        *ngFor="let user of usersService.users" class="list-group-item"
        [routerLink]="'/catalog/' + user.id"
      >
        <i
          class="fa" [ngClass]="{'fa-venus': user.gender === 'F', 'fa-mars': user.gender === 'M'}"
          [style.color]="user.gender === 'M' ? 'blue' : 'pink'"
        ></i>
        {{ user.name }} - {{user.age}}
        <app-gmap [address]="user.city" [showAddress]="false"></app-gmap>
        <i class="fa fa-trash pull-right" 
           (click)="usersService.deleteHandler(user, $event)"></i>

      </div>
    </div>  
  `,
  /*providers: [
    UsersService
  ],*/
})
export class CatalogComponent {
  constructor(public usersService: UsersService) {
    usersService.getUsers();
  }
}


// propagation
// catalog non singleton
