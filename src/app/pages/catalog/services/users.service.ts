import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class UsersService {
  users: User[] = [];

  constructor(private http: HttpClient) {}

  getUsers(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe((result) => {
        this.users = result;
      });
  }

  addUser(form: NgForm): void {
    this.http.post<User>('http://localhost:3000/users', form.value)
      .subscribe((res) => {
        this.users.push(res);
        form.reset();
      });
  }


  deleteHandler(userToDelete: User, event: MouseEvent): void {
    event.stopPropagation();
    this.http.delete('http://localhost:3000/users/' + userToDelete.id)
      .subscribe(() => {
        this.users = this.users.filter(user => user.id !== userToDelete.id)
      })
  }
}
