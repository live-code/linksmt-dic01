import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <pre>{{themeService.value}}</pre>
    
    <button (click)="themeService.setValue('dark')">
      <i class="fa fa-arrow-circle-o-right"
         *ngIf="themeService.value === 'dark'"></i>Dark
    </button>
    
    <button (click)="themeService.setValue('light')">
      <i class="fa fa-arrow-circle-o-right"
         *ngIf="themeService.value === 'light'"></i>Light
    </button>
  `,

})
export class SettingsComponent {

  constructor(public themeService: ThemeService) {}

}
