import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { delay, retry } from 'rxjs/operators';

@Component({
  selector: 'app-catalog-details',
  template: `
    
    <div class="alert alert-danger" *ngIf="error">PRODUCT NOT AVAILABLE </div>
    
    <div style="text-align: center">
     <i *ngIf="!user && !error" class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    
    <h3>{{user?.name}}</h3>
    <h5>{{user?.age}}</h5>
    <img *ngIf="user" [src]="'https://www.mapquestapi.com/staticmap/v5/map?&size=300,200&center=' + user?.city + '&key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn'" alt="">
  `,
})
export class CatalogDetailsComponent implements OnInit {
  user: User;
  error: boolean;

  constructor(
    activatedRoute: ActivatedRoute,
    http: HttpClient
  ) {
    const id = activatedRoute.snapshot.params.id;
    http.get<User>('http://localhost:3000/users/' + id)
      .pipe(delay(1000))
      .subscribe(
        res => this.user = res,
        (err) => this.error = true
      );
  }

  ngOnInit(): void {
  }

}
