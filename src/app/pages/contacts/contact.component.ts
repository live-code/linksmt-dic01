import { Component } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-contacts',
  template: `
    <h1>Contacts</h1>
    <app-gmap address="Trieste"></app-gmap>
    <app-gmap address="Lecce, Piazza S.Oronzo"></app-gmap>
    
  `
})
export class ContactComponent {

}
