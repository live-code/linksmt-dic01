import { Component, OnInit } from '@angular/core';
import { Country } from '../../model/country';

@Component({
  selector: 'app-home',
  template: `
    <app-tabbar 
      [items]="countries"
      [active]="selectedCity"
      (tabClick)="selectCity($event)"
    ></app-tabbar>
    
    <pre>{{selectedCity?.desc}}</pre>
    <app-gmap *ngIf="selectedCity" [address]="selectedCity?.name"></app-gmap>
    
    <hr>
    <button (click)="selectedCity = countries[2]">go to germany</button>

    <app-card 
      (iconClick)="openUrl('http://www.google.com')" 
      label="Google Profile" icon="fa fa-link"
    >
      <div class="row">
        <div class="col">
          <app-card title="left"></app-card>
        </div>
        <div class="col">
          <app-card title="left"></app-card>
        </div>
      </div>
    </app-card>
    
    <app-card 
      (iconClick)="openUrl('http://www.adobe.com')" 
      label="Linkedin Profile" icon="fa fa-link"
    >
      <app-gmap address="Milan"></app-gmap>
    </app-card>
    <app-card 
      (iconClick)="doSomething()" 
      label="Mario" icon="fa fa-times" theme="dark"
    ></app-card>
  `,
})
export class HomeComponent {
  countries: Country[] = [
    { id: 1, name: 'Italy', desc: 'bla bla'},
    { id: 2, name: 'Spain', desc: 'bla bla 1'},
    { id: 3, name: 'Germany', desc: 'bla bla 2'},
  ];
  selectedCity: Country = this.countries[2];

  selectCity(value: Country): void {
    this.selectedCity = value;
    console.log('selected city', value)
  }

  openUrl(url: string): void {
    window.open(url)
  }

  doSomething(): void {
    console.log('do something');
  }

}
