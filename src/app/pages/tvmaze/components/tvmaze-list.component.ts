import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series } from 'src/app/model/series';

@Component({
  selector: 'app-tvmaze-list',
  template: `
    <div class="grid">
      <div *ngFor="let series of result" class="grid-item">
        <div class="movie"
             (click)="clickItem.emit(series)"
             style="border: 2px solid red"
             [style.border]="series.score > 15 ? '5px solid green' : null"
             [ngClass]="{
              'high': series.score > 15,
              'low': series.score <= 15
             }"
             [class.high]="series.score > 15"
             [class.low]="series.score <= 15"
        > 
          <img
            *ngIf="series.show.image"
            [src]="series.show.image.medium" alt="">
          <div class="noImage" *ngIf="!series.show.image">
            NO IMAGE
          </div>
          <div class="movieText">{{series.show.name}}</div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./tvmaze-list.component.css']
})
export class TvmazeListComponent {
  @Input() result: Series[];
  @Output() clickItem: EventEmitter<any> =  new EventEmitter<any>();
}
