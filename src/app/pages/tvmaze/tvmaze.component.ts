import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schedule, Series, Show } from '../../model/series';

@Component({
  selector: 'app-tvmaze',
  template: `
    
    <form #f="ngForm" (submit)="search(f.value.text)"> 
      <input type="text" class="form-control" placeholder="Search Series" 
             [ngModel]="searchTxt" name="text"
             required minlength="3"
      >
      <button type="submit" [disabled]="f.invalid">SEARCH</button>
    </form>
    
    <app-tvmaze-list  
      [result]="result"
      (clickItem)="itemClickHandler($event)"
    ></app-tvmaze-list>
    

    <div class="wrapper" *ngIf="showDetails">
      <div class="content">
        <h2>{{showDetails.name}}</h2>
        <img [src]="showDetails.image.original" width="100%">
        <div [innerHTML]="showDetails.summary"></div>
        <div class="tag" *ngFor="let gen of showDetails.genres">{{gen}}</div>
      </div>
      <i class="fa fa-times closeButton" (click)="closeShowDetails()"></i>
    </div>
  `,
  styleUrls: ['./components/tvmaze-modal.component.css']
})
export class TvmazeComponent {
  result: Series[];
  showDetails: Show;
  searchTxt = 'viking';

  constructor(private http: HttpClient) {
    this.search(this.searchTxt);
  }

  search(text: string): void {
    this.http.get<Series[]>('http://api.tvmaze.com/search/shows?q=' + text)
      .subscribe(res => this.result = res)
  }

  itemClickHandler(series: Series): void {
    this.showDetails = series.show;
  }

  closeShowDetails(): void {
    this.showDetails = null;
  }
}
