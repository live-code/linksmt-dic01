import { Component, OnInit } from '@angular/core';
import { ThemeService } from './theme.service';
import { UsersService } from '../pages/catalog/services/users.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      style="border: 1px solid #222; padding: 10px;  margin: 10px"
      [style.background-color]="themeService.value === 'dark' ? '#222':'#dedede'"
    >
      <button routerLink="home" >home</button>
      <button routerLink="catalog" >catalog</button>
      <button routerLink="contacts" >contacts</button>
      <button routerLink="settings" >settings</button>
      <button routerLink="tvmaze" >tvmaze</button>
      
      <div class="pull-right">
        {{usersService.users.length}} users
      </div>
    </div>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(public themeService: ThemeService, public usersService: UsersService
  ) {}

  ngOnInit(): void {
  }

}
