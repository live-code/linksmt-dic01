import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  value = 'light';

  setValue(theme: string): void {
    this.value = theme;
  }
}
