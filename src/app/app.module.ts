import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ContactComponent } from './pages/contacts/contact.component';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { HomeComponent } from './pages/home/home.component';
import { RouterModule } from '@angular/router';
import { CatalogDetailsComponent } from './pages/catalog-details/catalog-details.component';
import { NavbarComponent } from './core/navbar.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { UsersService } from './pages/catalog/services/users.service';
import { TvmazeComponent } from './pages/tvmaze/tvmaze.component';
import { CardComponent } from './shared/components/card.component';
import { GmapComponent } from './shared/components/gmap.component';
import { TabbarComponent } from './shared/components/tabbar.component';
import { TvmazeListComponent } from './pages/tvmaze/components/tvmaze-list.component';
import { TvmazeFormComponent } from './pages/tvmaze/components/tvmaze-form.component';
import { TvmazeModalComponent } from './pages/tvmaze/components/tvmaze-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    CatalogComponent,
    HomeComponent,
    CatalogDetailsComponent,
    NavbarComponent,
    SettingsComponent,
    TvmazeComponent,
    CardComponent,
    GmapComponent,
    TabbarComponent,
    TvmazeListComponent,
    TvmazeFormComponent,
    TvmazeModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'contacts', component: ContactComponent },
      { path: 'catalog', component: CatalogComponent },
      { path: 'catalog/:id', component: CatalogDetailsComponent },
      { path: 'home', component: HomeComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'tvmaze', component: TvmazeComponent },
      { path: '', component: HomeComponent },
    ])
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
